import { createRouter, createWebHistory } from "vue-router";

import { Modal } from "@/components";
import { Home } from "@/views";

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
    children: [
      {
        path: ":modalId",
        component: Modal,
        props: true,
      },
    ],
  },
];

export default createRouter({
  history: createWebHistory(import.meta.env.BASE_UR),
  routes,
});
